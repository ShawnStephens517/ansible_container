# ansible-container

[![Main pipeline status](https://gitlab.com/ShawnStephens517/ansible_container/badges/main/pipeline.svg)](https://gitlab.com/ShawnStephens517/ansible_container/-/commits/main) 

[![Slim pipeline status](https://gitlab.com/ShawnStephens517/ansible_container/badges/slim/pipeline.svg)](https://gitlab.com/ShawnStephens517/ansible_container/-/commits/slim) 

Ansible Containerized. 
 
    - Use as a container in Gitlab CI/CD pipelines
    - Update requirements for any pip packages needed
    - Update DockerFile or Build off the container hosted here for any additional collections, modules, etc...
